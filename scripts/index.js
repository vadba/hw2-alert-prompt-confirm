// Теоретичні питання

//     1. Які існують типи даних у Javascript?
//         1. Строка. 2. Число. 3 Булевий. 4. Null. 5. undefined. 6. Об`єкт. 7. symbol. 8. bigInt. 9. Function. 

//     2. У чому різниця між == і ===?
//         == - порівняння значень, при цьому не відбувається порівняння типів даних. Не строге порівняння.
//         === - порівняння значень, при цьому відбувається порівняння типів даних. Строге порівняння.

//     3. Що таке оператор?
//         Символ, який здійснює дію з двома значеннями або змінними. Є математичні оператори, оператори порівняння, логічні оператори. Наприклад: ===, ==, !==, +, *, /, -, !.

//Завдання

let name = prompt('Введіть своє ім\`я');

if (name === '' || !isNaN(+name)) {
    name = prompt('Введіть своє ім\`я', name);
}

let age = prompt('Введіть свій вік');

if (age === '' || age === null || isNaN(+age)) {
    age = prompt('Введіть свій вік', age);
}

if (age < 18) {
    alert('You are not allowed to visit this website');
} else if (age >= 18 && age <= 22) {
    const modConfirm = confirm('Are you sure you want to continue?');
    if (modConfirm) {
        alert('Welcome, ' + name);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert('Welcome, ' + name);
}